# katapultwoo
Note:
This project wraps the Katapult preapproval/checkout online plugin. You'll need to have the ng-katapult, katapult-js and lms platform projects running alongside this.

1) Setup LAMP environment & Install wordpress
You can either install and configure Apache, PHP, MySQL or go the easier route of installing MAMP:
https://codex.wordpress.org/Installing_WordPress_Locally_on_Your_Mac_With_MAMP (Follow Step 1-4)
2) Install WooCommerce plugin under Plugins 
3) Git clone this project to this location:
/wordpress/wp-content/plugins/katapult
If you're using MAMP the full path should be:
/Applications/MAMP/htdocs/wordpress/wp-content/plugins/katapult
Within this folder you should see "katapult.php"  and zip the katapult folder (katapult.zip)
4) Visit "http://localhost/wordpress/wp-admin" and log in with your username/password
5) Visit "http://localhost/wordpress/wp-admin/plugins.php" and click Add New --> Upload Plugin --> Choose File (Upload katapult.zip)
6) Ensure that "WooCommerce Katapult" exists as a plugin. Activate it if it is not yet activated. If it does not appear, verify that Step 5 is complete and that there are no errors in the PHP error log.
7) In the left hand menu, select WooCommerce --> Settings and select Katapult from the checkout settings window on the right side
8) Ensure the appropriate tokens are set for the environment you're working within. When testing with a local Katapult JS branch you should set the Environment to "http://localhost:8001"
9) Visit the storefront: http://localhost/wordpress/shop/
10) Add product to cart --> proceed to checkout --> select Katapult as a payment option --> checkout
