<?php

class WC_Payment_Gateway_Katapult extends WC_Payment_Gateway
{
    private static $instance;

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * Constructor for the gateway.
     */
    public function __construct()
    {
        $this->id = 'katapult';
        $this->has_fields = false;
        $this->method_title = 'Katapult';
        $this->icon = plugins_url('assets/images/icon.png', __FILE__);
        $this->title = 'Katapult';
        $this->method_description = 'Buy now. Pay over time with Katapult';
        $this->dev_mode = false;

        // Load the settings
        $this->init_form_fields();
        $this->init_settings();

        // Get settings
        $this->enable_for_virtual = true;

    }


    public function init()
    {
        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
        add_action('woocommerce_api_' . strtolower(get_class($this)), array($this, 'callback_handler'));
        add_action('woocommerce_before_checkout_form', array($this, 'add_jscript'));
        add_action('woocommerce_checkout_update_order_review', array($this, 'taxexempt_checkout_update_order_review'));
        add_action('add_meta_boxes', array($this, 'add_meta_boxes'));
        add_action('wp_ajax_cancel_order', array($this, 'cancel_order_ajax'));
        add_action('admin_enqueue_scripts', array($this, 'admin_scripts'));
        add_action('woocommerce_product_options_general_product_data', array($this, 'create_leasable_field'));
        add_action('woocommerce_process_product_meta', array($this, 'product_leasable_field_save'));
        add_action('woocommerce_variation_options_pricing', array($this, 'create_variable_product_leasable_field'), 10, 3);
        add_action('woocommerce_save_product_variation', array($this, 'product_variation_leasable_field_save'), 10, 2);
    }

    public function create_leasable_field() {

        $args = array(
            'id' => 'leasable',
            'label' => __( 'Leasable with Katapult', 'woocommerce' ),
            'desc_tip' => true,
            'description' => __( 'Check if product is leasable with Katapult.', 'woocommerce' )
        );
        woocommerce_wp_checkbox( $args );
    }

    public function product_leasable_field_save($post_id) {
        $leasable_field = isset( $_POST['leasable'] ) ? 'yes' : 'no';
        if (!empty($leasable_field))
            update_post_meta($post_id, 'leasable', esc_attr($leasable_field));
    }

    public function create_variable_product_leasable_field($loop, $variation_data, $variation) {
        $args = array(
            'id' => 'leasable_variable',
            'label' => __( 'Leasable with Katapult', 'woocommerce' ),
            'desc_tip' => true,
            'description' => __( 'Check if product is leasable with Katapult.', 'woocommerce' ),
            'value' => get_post_meta($variation->ID, 'leasable_variable', true)
        );
        woocommerce_wp_checkbox( $args );
    }

    public function product_variation_leasable_field_save($variation_id, $i) {
        $leasable_field = isset( $_POST['leasable_variable'] ) ? 'yes' : 'no';
        if (!empty($leasable_field))
            update_post_meta($variation_id, 'leasable_variable', esc_attr($leasable_field));
    }

    public function taxexempt_checkout_update_order_review()
    {
        if ($_POST['payment_method'] == 'katapult') {
            WC()->customer->set_is_vat_exempt(true);
        } else {
            WC()->customer->set_is_vat_exempt(false);
        }
    }

    /**
     * Build the entire cart object in a way that the Katapult plugin
     * can understand.
     *
     * @return array
     */
    public function get_katapult_checkout_object()
    {
        $order_id = $_GET['order'];
        $order = new WC_Order($order_id);

        $katapult_checkout_object = array(
            'customer' => array(
                'billing' => array(
                    'first_name' => $order->billing_first_name,
                    'middle_name' => $order->billing_middle_name,
                    'last_name' => $order->billing_last_name,
                    'address' => $order->billing_address_1,
                    'address2' => $order->billing_address_2,
                    'country' => $order->billing_country,
                    'city' => $order->billing_city,
                    'state' => $order->billing_state,
                    'zip' => $order->billing_postcode,
                    'phone' => $order->billing_phone,
                    'email' => $order->billing_email
                ),
                'shipping' => array(
                    'first_name' => $order->shipping_first_name,
                    'middle_name' => $order->shipping_middle_name,
                    'last_name' => $order->shipping_last_name,
                    'address' => $order->shipping_address_1,
                    'address2' => $order->shipping_address_2,
                    'country' => $order->shipping_country,
                    'city' => $order->shipping_city,
                    'state' => $order->shipping_state,
                    'zip' => $order->shipping_postcode,
                    'phone' => $order->shipping_phone,
                    'email' => $order->shipping_email
                )
            ),
            'items' => $this->get_formatted_items_for_katapult(),
            'checkout' => array(
                'customer_id' => $order_id,
                'shipping_amount' => $order->get_total_shipping(),
                'discounts' => $this->get_formatted_discounts_for_katapult($order)
            ),
            'urls' => array(
                'return' => get_site_url() . '/wc-api/WC_Payment_Gateway_Katapult?order=' . $order_id,
                'cancel' => ''
            )
        );

        return $katapult_checkout_object;
    }

    /**
     * Iterates over the WooCommerce cart items and formats them in a way
     * that the Katapult plugin can understand.
     *
     * @return array
     */
    function get_formatted_items_for_katapult()
    {
        $processed_items = array();

        $items = WC()->cart->get_cart();

        foreach ($items as $item => $values) {
            $data = $values['data'];
            $product = $data->post;

            if ('WC_Product_Simple' === get_class($data)) {
                $product_with_sku = new WC_Product_Simple($data->id);
            }

            if ('WC_Product_Variation' === get_class($data)) {
                $product_with_sku = new WC_Product_Variation($data->variation_id);
            }

            if ('WC_Product_External' === get_class($data)) {
                $product_with_sku = new WC_Product_External($data->id);
            }

            $item = new stdClass();
            $item->sku = $product_with_sku->get_sku();
            $item->display_name = $product->post_title;
            $item->unit_price = $values['line_subtotal'] / $values['quantity'];
            $item->quantity = $values['quantity'];
            $item->leasable = ('yes' === $product->leasable || $product->leasable_variable) ? true : false;

            $processed_items[] = $item;

        }

        return $processed_items;
    }

    /**
     * Iterates over the WooCommerce cart discounts and formats them in a way
     * that the Katapult plugin can understand.
     *
     * @return array
     */
    public function get_formatted_discounts_for_katapult($order)
    {
        $coupons = $order->get_coupons();
        $processed_items = array();

        foreach ($coupons as $coupon) {
            $processed_items[] = array(
                'discount_name' => $coupon->get_code(),
                'discount_amount' => abs($coupon->get_discount())
            );
        }

        return $processed_items;
    }

    /**
     * After WooCommerce validates the checkout form we have it redirect
     * to itself with the 'katapult' query param. This lets us know it's time
     * to bootstrap the Katapult plugin.
     *
     * The cart, config JSON and Katapult JS snippet get embedded into the
     * page here.
     */
    public function add_jscript()
    {
        wp_enqueue_script('woocommerce_katapult_gateway', plugins_url('assets/js/katapult-gateway.js', __FILE__));

        if ($_GET['katapult'] == 1) {
            $katapult_checkout_object = $this->get_katapult_checkout_object();
            if (isset($katapult_checkout_object['customer']['shipping']['address'])) {
                $katapult_checkout_object['customer']['shipping'] = $katapult_checkout_object['customer']['billing'];
            };
            wp_enqueue_script('woocommerce_katapult', plugins_url('assets/js/katapult-checkout.js', __FILE__));
            wp_localize_script('woocommerce_katapult', 'katapultCart', $katapult_checkout_object);
        }
    }

    /**
     * Katapult only supports US customers and is not available in some states
     *
     * @return bool
     */
    function is_available()
    {
        $is_available = ('yes' === $this->enabled) ? true : false;
        if (!is_admin()) {
            $items = WC()->cart->get_cart();
            $is_leasable = false;
            $leasable_array = array();
            foreach ($items as $item => $values) {
                $product = $values['data']->post;
                if ('yes' === $product->leasable || 'yes' === $product->leasable_variable) {
                    array_push($leasable_array, 1);
                }
                else {
                    array_push($leasable_array, 0);
                }
            }

            if (in_array(1, $leasable_array)) {
                $is_leasable = true;

                if (in_array(0, $leasable_array)) {
                    $is_leasable = false;
                }
            }

            if (WC()->customer && ("US" !== WC()->customer->get_billing_country())) {
                $is_available = false;
            } elseif (WC()->cart->total < $this->get_option('min_order_total') || WC()->cart->total > $this->get_option('max_order_total')) {
                $is_available = false;
            } elseif (!$is_leasable) {
                $is_available = false;
            } else {
                $katapult_unavailable_states = ['NJ', 'MN', 'WI', 'WY'];
                $state = WC()->customer->get_billing_state();

                if (in_array($state, $katapult_unavailable_states)) {
                    $is_available = false;
                }
            }
        } else {
            $is_available = false;
        }

        return $is_available;
    }

    /**
     * Initialise Gateway Settings Form Fields.
     */
    public function init_form_fields()
    {
        $this->form_fields = array(
            'enabled' => array(
                'title' => __('Enable Katapult', 'woocommerce'),
                'label' => __('Enable Katapult Preapproval/Checkout', 'woocommerce'),
                'type' => 'checkbox',
                'description' => '',
                'default' => 'no'
            ),
            'environment' => array(
                'title' => __('Environment', 'woocommerce'),
                'type' => 'text',
                'description' => __('Which Katapult domain to use as environment', 'woocommerce'),
                'default' => __('n/a', 'woocommerce')
            ),
            'private_token' => array(
                'title' => __('Private API key', 'woocommerce'),
                'type' => 'text',
                'description' => __('This is the retailer API key that was provided to you by Katapult', 'woocommerce'),
                'default' => __('', 'woocommerce')
            ),
            'public_token' => array(
                'title' => __('Public API key', 'woocommerce'),
                'type' => 'text',
                'description' => __('This is the retailer API key that was provided to you by Katapult', 'woocommerce'),
                'default' => __('', 'woocommerce')
            ),
            'min_order_total' => array(
                'title' => __('Minimum Order Total', 'woocommerce'),
                'type' => 'text',
                'description' => __('The minimum value for order', 'woocommerce'),
                'default' => __('300.00', 'woocommerce')
            ),
            'max_order_total' => array(
                'title' => __('Maximum Order Total', 'woocommerce'),
                'type' => 'text',
                'description' => __('The maximum value for order', 'woocommerce'),
                'default' => __('4500.00', 'woocommerce')
            ),
        );
    }

    /**
     * Process the payment and return the result.
     *
     * @param int $order_id
     * @return array
     */
    public function process_payment($order_id)
    {
        $order = wc_get_order($order_id);

        $redirect_url = add_query_arg(
            array(
                'katapult' => '1',
                'order' => $order_id,
                'nonce' => wp_create_nonce('katapult-checkout-order-' . $order_id)
            ), get_permalink(wc_get_page_id('checkout'))
        );

        return array(
            'result' => 'success',
            'redirect' => $redirect_url
        );
    }

    /**
     * Called both when the Katapult plugin successfully originates a lease
     * as well as when the user completes the flow.
     *
     * If the request method is POST it means Katapult is attempting to update
     * the status of an order in Woo. Decode the response and update the
     * appropriate order.
     *
     * If the request method is GET, mark payment as processing and redirect to the Woo thank you
     * page.
     *
     * @param int $order_id
     * @return array
     */
    public function callback_handler()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $katapult_response = json_decode(file_get_contents('php://input'));
            $order = new WC_Order($katapult_response->customer_id);

            $status = $this->get_status($katapult_response->uid);

            if ($status != 'pending' && $status != 'current') {
                return;
            }

            $order->payment_complete($katapult_response->uid);

        } else {
            $order_id = $_GET['order'];
            $order = new WC_Order($order_id);
            $order->payment_complete($order->get_transaction_id());
            header('Location:' . $this->get_return_url($order));
        }
    }

    /**
     * If this is a Katapult order, initialize the Katapult status
     * metabox in the admin.
     */
    public function add_meta_boxes()
    {
        $meta = get_post_meta($_GET['post']);

        if (!array_key_exists('_cart_hash', $meta)) {
            return;
        }

        $order = new WC_Order($_GET['post']);
        $payment_method = wc_get_payment_gateway_by_order($order)->title;

        if ($payment_method != 'Katapult') return;

        $transaction_id = $order->get_transaction_id();
        $status = $this->get_status($transaction_id);

        if (!$transaction_id || !$status) return;

        add_meta_box(
            'woocommerce-order-my-custom',
            __('Katapult Status'),
            array($this, 'get_katapult_status_meta_box'),
            'shop_order',
            'side',
            'default',
            array($status)
        );
    }

    /**
     * Gets the current status of a Katapult order.
     */
    public function get_status($transaction_id)
    {
        $application = $this->make_authenticated_request('application/' .
            $transaction_id . '/', false, 'ng');

        return $application->status;
    }

    /**
     * Determine environment, API type etc and make authenticated
     * request to Katapult.
     */
    public function make_authenticated_request($resource, $is_private, $api)
    {
        $token = $is_private ? $this->get_option('private_token') : $this->get_option('public_token');
        $host = $this->dev_mode ? 'http://localhost:8000' : $this->get_option('environment');
        $host .= $api == 'ng' ? '/api/ng/katapultjs/' : '/api/v3/';

        $options = array(
            'method' => 'GET',
            'headers' => array(
                'Authorization' => 'Bearer ' . $token,
                'Content-Type' => 'application/json'
            )
        );

        $url = $host . $resource;
        $response = wp_remote_get($url, $options);
        return json_decode($response['body']);
    }

    /**
     *
     */
    public function cancel_order_ajax()
    {
        $post_id = $_POST['data'];
        $order = new WC_Order($post_id);
        $transaction_id = $order->get_transaction_id();

        $application = $this->make_authenticated_request('application/' .
            $transaction_id . '/cancel_order/', true, 'v3');

        $transaction_id = $order->get_transaction_id();
        $status = $this->get_status($transaction_id);

        if ($status == 'canceled') {
            $order->update_status('cancelled');
        }
    }

    /**
     *
     */
    public function admin_scripts($hook)
    {
        if ('post.php' != $hook) {
            return;
        }

        wp_enqueue_script('katapult-status', plugins_url('assets/js/katapult-status.js', __FILE__), 'jQuery');
    }

    public function get_icon()
    {
        $icon_html = '<img src="' . plugins_url('assets/images/icon.png', __FILE__) . '"><a  style="float: right; font-size: .83em;" onclick="javascript:window.open(\'https://s3.amazonaws.com/about-katapult/index.html\',\'WIKatapult\',\'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1060, height=700\'); return false;" title="' . esc_attr__('What is Katapult?', 'woocommerce') . '">' . esc_attr__('What is Katapult?', 'woocommerce') . '</a>';
        return apply_filters('woocommerce_gateway_icon', $icon_html, $this->id);
    }

    /**
     * Build the HTML/CSS for the Katapult status metabox in the
     * edit order admin.
     */
    function get_katapult_status_meta_box($data, $box)
    {
        $status = $box['args']['0'];
        ?>
        <style>
            .katapult-status {
                color: #0085ba;
                display: inline-block;
            }

            .katapult-status.canceled {
                color: red;
            }

            .katapult-status-container .button {
                float: right;
                margin-top: 10px;
            }

            .katapult-status-container.canceled .button {
                display: none;
            }
        </style>

        <div class="katapult-status-container <?php echo $status ?>">
            <h1 class="katapult-status <?php echo $status ?>"><?php echo $status; ?></h1>
            <a class="cancel-order button save_order button-primary">Cancel order</a>
        </div>

    <?php }

}