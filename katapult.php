<?php
/*
Plugin Name: WooCommerce Katapult
Description: Enable Katapult Checkout for WooCommerce 3.X
Version:     3.4
Author:      Cognical, Renars Grebezs <info@scandiweb.com>
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: wporg
Domain Path: /languages
*/
class Loader
{
    private static $instance = null;
    private $gateway = null;

    private function __clone()
    {
    }

    private function __wakeup()
    {
    }

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function get_gateway()
    {
        if ($this->gateway == null && class_exists('WC_Payment_Gateway')) {
            include( plugin_dir_path( __FILE__ ) . 'inc/katapult-gateway.php');
            $this->gateway = WC_Payment_Gateway_Katapult::getInstance();
            $this->gateway->init();
        }
        return $this->gateway;
    }

    public function wc_payment_gateway_katapult($methods)
    {
        $methods[] = 'WC_Payment_Gateway_Katapult';
        return $methods;
    }

    public function __construct()
    {
        add_action('plugins_loaded', array($this, 'get_gateway'));
        add_filter('woocommerce_payment_gateways', array($this, 'wc_payment_gateway_katapult'));
        add_action('wp', array($this, 'init'));
    }

    public function init()
    {
        $this->is_checkout = is_checkout();
        $katapult_config = array(
            'environment' => $this->get_gateway()->get_option('environment'),
            'api_key' => $this->get_gateway()->get_option('public_token')
        );

        wp_register_script('katapult_config', false);
        wp_localize_script('katapult_config', '_katapult_config', $katapult_config);
        wp_enqueue_script('katapult_config');

        $dependencies = array();
        if ($this->is_checkout) {
            $dependencies[] = 'woocommerce_katapult';
        }

        wp_enqueue_script('katapult_js', plugins_url('assets/js/katapult.js', __FILE__), $dependencies, '', true);
    }
}

$GLOBALS['loader'] = Loader::getInstance();
